<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $discord_id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $accept_terms;

    /**
     * @ORM\Column(type="json")
     */
    private $role = [];

    /**
     * @ORM\ManyToMany(targetEntity=Group::class, inversedBy="users")
     */
    private $inGroup;

    public function __construct()
    {
        $this->inGroup = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDiscordId(): ?int
    {
        return $this->discord_id;
    }

    public function setDiscordId(int $discord_id): self
    {
        $this->discord_id = $discord_id;

        return $this;
    }

    public function getAcceptTerms(): ?bool
    {
        return $this->accept_terms;
    }

    public function setAcceptTerms(bool $accept_terms): self
    {
        $this->accept_terms = $accept_terms;

        return $this;
    }

    public function getRole(): ?array
    {
        return $this->role;
    }

    public function setRole(array $role): self
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @return Collection|Group[]
     */
    public function getInGroup(): Collection
    {
        return $this->inGroup;
    }

    public function addInGroup(Group $inGroup): self
    {
        if (!$this->inGroup->contains($inGroup)) {
            $this->inGroup[] = $inGroup;
        }

        return $this;
    }

    public function removeInGroup(Group $inGroup): self
    {
        $this->inGroup->removeElement($inGroup);

        return $this;
    }
}
